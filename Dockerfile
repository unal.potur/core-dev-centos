FROM registry.gitlab.com/kanalici/centos-systemd
MAINTAINER Evren KANALICI <evren.kanalici [at] nevalabs [dot] com>

LABEL version="1.5"
LABEL description="core-dev centos environment"

RUN yum list updates
RUN yum -y install centos-release-scl
RUN yum -y install devtoolset-6 bzip2 cmake git svn

RUN scl enable devtoolset-6 bash
ENV PATH $PATH:/opt/rh/devtoolset-6/root/usr/bin

ADD boost.sh /tmp/boost.sh
RUN chmod +x /tmp/boost.sh
RUN /tmp/boost.sh

COPY jasper_assert_patch /opt/jasper_assert_patch
RUN yum -y install patch

ADD opencv.sh /tmp/opencv.sh
RUN chmod +x /tmp/opencv.sh
RUN /tmp/opencv.sh

# install openssl
ENV OPENSSL_VERS 1.1.1k
#ENV OPENSSL_VERS 1.0.2k

RUN yum -y install openssl-devel-${OPENSSL_VERS}

RUN yum -y install \
    libtool \
    libevent-devel

ADD install-thrift.sh /tmp/install-thrift.sh
RUN chmod +x /tmp/install-thrift.sh
RUN /tmp/install-thrift.sh

# cmake3
RUN yum -y remove cmake
RUN yum -y install epel-release
RUN yum -y install cmake3 && ln -s /usr/bin/cmake3 usr/bin/cmake

# install libmongoc
ADD install-libmongoc.sh /tmp/install-libmongoc.sh
RUN chmod +x /tmp/install-libmongoc.sh
RUN /tmp/install-libmongoc.sh

# install mongocxx
ADD install-mongocxx.sh /tmp/install-mongocxx.sh
RUN chmod +x /tmp/install-mongocxx.sh
RUN /tmp/install-mongocxx.sh

# install git-lfs
RUN cd /tmp \
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | bash \
    && yum -y install git-lfs \
    && git lfs install >/dev/null 2>&1

# install dev. tools
RUN yum -y install \
    bash-completion \
    nano \
    tree \
    elfutils-devel \
    libcurl-devel

# sentinel
RUN yum -y install glibc.i686 initscripts
COPY aksusbd-7.51.1-i386 /opt/aksusbd-7.51.1-i386
RUN cd /opt/aksusbd-7.51.1-i386 \
    && ./dinst \
    && cp hasplm.ini /etc/hasplm/ \
    && hasplmd -s

# cleanup
RUN yum clean all

# set locale
RUN echo 'LC_CTYPE="en_US.UTF-8"' >> /etc/sysconfig/i18n
RUN localedef -i en_US -f UTF-8 en_US.UTF-8

# update LD_LIBRARY_PATH
RUN ldconfig

# User env. variables
ENV DEV_ENV_LONG      "centos-7"
#ENV DEV_ENV_LONG      "centos-8"
ENV DEV_ENV_SHORT     "centos"
ENV NEVA_DATA_ROOT    "/mnt/data/"

ENTRYPOINT /bin/bash

