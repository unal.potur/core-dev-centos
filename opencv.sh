#! /bin/bash
set -e # fail fast

# download opencv
OPENCV_VERS=3.4.0
pushd /tmp
if [[ ! -d opencv ]]; then
    curl -SL -o opencv.zip https://github.com/opencv/opencv/archive/${OPENCV_VERS}.zip
    [ $(sha1sum opencv.zip | awk '{print $1}') == 'b60908c5f52c7897be40e20f4b064911dbe8de0d' ]
    unzip opencv.zip >/dev/null 2>&1 && mv opencv-${OPENCV_VERS} opencv
fi
if [[ ! -d opencv_contrib ]]; then
    curl -SL -o opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERS}.zip
    [ $(sha1sum opencv_contrib.zip | awk '{print $1}') == '7aafd8ce8cf2de62fe686f6ea21410b0c9c1c978' ]
    unzip opencv_contrib.zip >/dev/null 2>&1 && mv opencv_contrib-${OPENCV_VERS} opencv_contrib
fi

pushd opencv

# jasper_assert patch
pushd 3rdparty
pushd libjasper
yes | patch < /opt/jasper_assert_patch/jas_image.patch
popd
popd

# configure
mkdir -p release
pushd release
cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D OPENCV_EXTRA_MODULES_PATH=/tmp/opencv_contrib/modules \
    -D BUILD_JASPER=ON ..

# build & install opencv
make -j4
make install
popd
popd
popd

rm -rf /tmp/opencv*
echo "/usr/local/lib64" >> /etc/ld.so.conf.d/opencv-3.4.0.conf